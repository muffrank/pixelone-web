import React from "react";
import { Pixelheader, NavButton, MLink } from "../styled";
import Footer from "./common/Footer";
import PixelNav from "./common/Nav";
import EighthSection from "./sections/EighthSection";

const Customer = () => {
  return (
    <>
      <PixelNav />
      <EighthSection />
      <Footer />
    </>
  );
};

export default Customer;
