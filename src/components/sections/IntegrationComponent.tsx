import React from "react";
import {
  ThirdSectionContainer,
  LeftOfThirdSection,
  ThirdMainHeading,
  Border,
  ThirdSectionDiv,
  ChekCircle,
  SimpleDiv,
  WhiteHeading,
  WHiteParagraph,
  Div,
  RightOfThirdSection,
  ThirdSectionImg,
  BigHeading,
  BigParagraph,
  BottomOfFourthSection,
  FourthSectionContainer,
  FourthSectionDiv,
  FourthSectionImg,
  SmallHeading,
  SmallParagraph,
  TopOfFourthSection,
} from "../../styled";

import shopify from "../images/shopify.png";
import woocommerce from "../images/woocommerce.png";
import daraz from "../images/daraz.png";
import magento from "../images/magento.png";
import tcs from "../images/tcs.png";
import leopard from "../images/leopard.jpeg";
import forrun from "../images/forrun.png";
import mnp from "../images/mnp.png";
import callcurior from "../images/call-courier.jpg";
import tezz from "../images/tezz.png";
import pkpost from "../images/pkpost.png";

function IntegrationComponent() {
  return (
    <FourthSectionContainer>
      <TopOfFourthSection>
        <BigHeading>Supported Integrations</BigHeading>
      </TopOfFourthSection>
      <BottomOfFourthSection>
        <FourthSectionDiv>
          <FourthSectionImg src={shopify} />
          <SmallHeading>Shopify</SmallHeading>
          <SmallParagraph>
            Manage stock levels, incoming orders, customer contacts, product
            images, descriptions and moreover multiple Shopify stores.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={woocommerce} />
          <SmallHeading>Woocommerce</SmallHeading>
          <SmallParagraph>
            Manage sales and purchase orders, invoices, payment terms and
            discounts on your WooCommerce store.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={daraz} />
          <SmallHeading>Daraz</SmallHeading>
          <SmallParagraph>
            Manage sales, products, stock, payments on your Daraz store. Learn
            more
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={magento} />
          <SmallHeading>Magento</SmallHeading>
          <SmallParagraph>
            Manage your inventory across multiple Magento stores, keep track of
            sales and purchase orders or update product descriptions, prices and
            discounts easily.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={tcs} />
          <SmallHeading>TCS</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={leopard} />
          <SmallHeading>Leopard</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={forrun} />
          <SmallHeading>Forrun</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <SmallHeading>Trax</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={mnp} />
          <SmallHeading>MNP</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={callcurior} />
          <SmallHeading>Call Courier</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={tezz} />
          <SmallHeading>TEZZ</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={pkpost} />
          <SmallHeading>Pakistan Post</SmallHeading>
          <SmallParagraph>
            Pixel One helps you automate your shipping so you can create{" "}
            consignments with one click and save hours of work every day.
          </SmallParagraph>
        </FourthSectionDiv>
      </BottomOfFourthSection>
    </FourthSectionContainer>
  );
}

export default IntegrationComponent;
