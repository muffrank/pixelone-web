import React from "react";
import inventrycontrol from "../images/in-depth-features/inventory-control.svg";
import inventryoptimization from "../images/in-depth-features/inventory-optimization.svg";
import warehouse from "../images/in-depth-features/warehouse-management.svg";
import ordermanagment from "../images/in-depth-features/order-management.svg";
import multipulesale from "../images/in-depth-features/multichannel-sales.svg";
import salesreports from "../images/in-depth-features/sales-reports.svg";
import shiping from "../images/in-depth-features/shipping.svg";
import purchaseordermanagement from "../images/in-depth-features/purchase-order-management.svg";
import orderfulfillment from "../images/in-depth-features/order-fulfillment.svg";
import accounting from "../images/in-depth-features/accounting.svg";
import inventorymanagementreports from "../images/in-depth-features/inventory-management-reports.svg";

import {
  BigHeading,
  BigParagraph,
  BottomOfFourthSection,
  FourthSectionContainer,
  FourthSectionDiv,
  FourthSectionImg,
  ParagraphContainerOfSecend,
  SmallHeading,
  SmallParagraph,
  TopOfFourthSection,
} from "../../styled";
function FourthSection() {
  return (
    <FourthSectionContainer>
      <TopOfFourthSection>
        <BigHeading>In Depth Features</BigHeading>
        <ParagraphContainerOfSecend>
          <BigParagraph>
            For an eCommerce business, proper management of inventory, orders
            and accounts is crucial to ensuring the efficient operation of your
            business. Pixel One lets business owners manage your inventory,
            orders, accounts and customers from one place. Allowing you to take
            orders, sell more, and work smarter, Pixel One's inventory
            management system means you have more time to focus on growing your
            eCommerce business. Take a look at our software features below.
          </BigParagraph>
        </ParagraphContainerOfSecend>
      </TopOfFourthSection>
      <BottomOfFourthSection>
        <FourthSectionDiv>
          <FourthSectionImg src={inventrycontrol} />
          <SmallHeading>Inventory Control</SmallHeading>
          <SmallParagraph>
            Improve stock and inventory tracking through automatically updated
            stock levels whenever sales and purchases are made. With Pixel One,
            you can manage your inventory across multiple warehouses.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={inventryoptimization} />
          <SmallHeading>Inventory Optimization</SmallHeading>
          <SmallParagraph>
            Pixel One's inventory management system allows you to have the right
            amount of stock at the right time. Streamline your internal
            operations through automated demand forecasting, inventory
            optimization, and reorder points.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={warehouse} />
          <SmallHeading>Warehouse Management</SmallHeading>
          <SmallParagraph>
            Manage multiple warehouses and inventory locations efficiently
            across all locations on a single platform. Pixel One lets you
            transfer stock between warehouses, receive and fulfill orders
            through specific warehouses, and more.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={ordermanagment} />
          <SmallHeading>Order Management</SmallHeading>
          <SmallParagraph>
            Synchronize your orders with your inventory and increase your
            business efficiency from order creation to fulfillment. You will be
            able to synchronize orders with your inventory and access sales
            reports, all in real time.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={multipulesale} />
          <SmallHeading>Multichannel Sales</SmallHeading>
          <SmallParagraph>
            Pixel One provides a central hub to manage inventory and orders for
            all your sales channels efficiently and effortlessly. You can sell
            on marketplaces, B2C and B2B eCommerce platforms, and on mobile.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={salesreports} />
          <SmallHeading>Sales Reports</SmallHeading>
          <SmallParagraph>
            Access and generate accurate sales reports that offer insights for
            better business decisions, all in real time. Our inventory
            management software allows you to generate sales history and order
            reports by customer, product, channel, location, and more.
          </SmallParagraph>
        </FourthSectionDiv>{" "}
        <FourthSectionDiv>
          <FourthSectionImg src={shiping} />
          <SmallHeading>Shipping</SmallHeading>
          <SmallParagraph>
            Simplify the order process for your customers with Pixel One
            shipping. Set up your shipping companies like TCS, Leopard, Trax,
            Forrun, Pakistan Post and more and automate the consignments
            creation and order tracking.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={purchaseordermanagement} />
          <SmallHeading>Purchase Order Management</SmallHeading>
          <SmallParagraph>
            Create, edit, and email purchase orders that update inventory levels
            automatically while ensuring data accuracy. You can also generate
            reorder reports so you know exactly when to order more inventory.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={orderfulfillment} />
          <SmallHeading>Order Fulfillment</SmallHeading>
          <SmallParagraph>
            Now you’re able to automate and control your order fulfillment in a
            greater volume and at lower costs. Integrate your channels,
            locations, and processes for greater visibility.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={accounting} />
          <SmallHeading>Accounting</SmallHeading>
          <SmallParagraph>
            Pixel One's accounting system integrates seamlessly with your
            inventory and order management systems. Automate your invoicing and
            gain greater visibility over costs and profits with our automated
            accounts integration.
          </SmallParagraph>
        </FourthSectionDiv>
        <FourthSectionDiv>
          <FourthSectionImg src={inventorymanagementreports} />
          <SmallHeading>Inventory Management Reports</SmallHeading>
          <SmallParagraph>
            Access accurate inventory reports that offer real time insights into
            stock movement. Our inventory management software allows you to run
            stock reorder reports, inventory stock on hand reports, and more.
          </SmallParagraph>
        </FourthSectionDiv>
      </BottomOfFourthSection>
    </FourthSectionContainer>
  );
}

export default FourthSection;
