import React from "react";
import {
  SixthSectionContainer,
  SixthSectionHeading,
  SixthSectionButton,
} from "../../styled";
function SixthSection() {
  return (
    <>
      <SixthSectionContainer>
        <SixthSectionHeading>
          Start A Full Featured Trial And Pick A Plan Later!
        </SixthSectionHeading>
        <SixthSectionButton
          href="https://pixelone.app/auth/login"
          target="_blank"
        >
          Start Free Trial
        </SixthSectionButton>
        <SixthSectionHeading>Call Us On +92 320 573 1765</SixthSectionHeading>
      </SixthSectionContainer>
    </>
  );
}

export default SixthSection;
