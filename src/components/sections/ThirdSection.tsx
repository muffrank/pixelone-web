import React from "react";
import img from "../images/saas2/we-offer/img.png";
import {
  ThirdSectionContainer,
  ThirdMainHeading,
  LeftOfThirdSection,
  RightOfThirdSection,
  Border,
  ThirdSectionDiv,
  ThirdSectionImg,
  WhiteHeading,
  WHiteParagraph,
  Div,
  SimpleDiv,
  PaperPlane,
  ChekCircle,
  ThirdSectionSimpleDiv,
} from "../../styled";
import { faCheckCircle, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
function ThirdSection() {
  return (
    <ThirdSectionContainer>
      <LeftOfThirdSection>
        {" "}
        <ThirdMainHeading>
          Pixel One Helps You To Take Control Of Your Business
        </ThirdMainHeading>
        <Border></Border>
        <ThirdSectionDiv>
          <ChekCircle icon={faCheckCircle} />
          <ThirdSectionSimpleDiv>
            <WhiteHeading>Visibility & Control</WhiteHeading>
            <WHiteParagraph>
              Pixel One offers inventory control, providing deep insights into
              business performance.
            </WHiteParagraph>
          </ThirdSectionSimpleDiv>
        </ThirdSectionDiv>
        <ThirdSectionDiv>
          <ChekCircle icon={faCheckCircle} />
          <ThirdSectionSimpleDiv>
            <WhiteHeading>Sell More</WhiteHeading>
            <WHiteParagraph>
              We create the tools you need to engage with more customers and
              more opportunities.
            </WHiteParagraph>
          </ThirdSectionSimpleDiv>
        </ThirdSectionDiv>
        <ThirdSectionDiv>
          <ChekCircle icon={faCheckCircle} />
          <ThirdSectionSimpleDiv>
            <WhiteHeading>Save Time</WhiteHeading>
            <WHiteParagraph>
              Automate time consuming tasks so you can concentrate on developing
              your brand.
            </WHiteParagraph>
          </ThirdSectionSimpleDiv>
        </ThirdSectionDiv>
        <ThirdSectionDiv>
          <ChekCircle icon={faCheckCircle} />
          <ThirdSectionSimpleDiv>
            <WhiteHeading>Work Smarter</WhiteHeading>
            <WHiteParagraph>
              All your inventory and order data is backed up, synced, and secure
              in the cloud.
            </WHiteParagraph>
          </ThirdSectionSimpleDiv>
        </ThirdSectionDiv>
      </LeftOfThirdSection>
      <RightOfThirdSection>
        <ThirdSectionImg src={img} />
      </RightOfThirdSection>
    </ThirdSectionContainer>
  );
}

export default ThirdSection;
