import React from "react";
import pixalimg from "../images/pixel-one.png";
import inventery from "../images/inventory-management.svg";
import order from "../images/orders-and-fulfillment.svg";
import relationship from "../images/relationships.svg";
import intellgence from "../images/intelligence.svg";
import {
  //--secend section
  SecendSectionContainer,
  TopOfSecendSection,
  BottomOfSecendSection,
  LeftOfBottom,
  RightOfBottom,
  BigHeading,
  BigParagraph,
  SmallHeading,
  SmallParagraph,
  Pixalimg,
  Div,
  SimpleDiv,
  SimpleImg,
  ParagraphContainerOfSecend,
} from "../../styled";
import {
  faAngleDoubleUp,
  faFontAwesome,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
function SecendSection() {
  return (
    <>
      <SecendSectionContainer>
        <TopOfSecendSection>
          <BigHeading>Core Features</BigHeading>
          <ParagraphContainerOfSecend>
            <BigParagraph>
              Pixel One is a powerful cloud-based eCommerce management software
              for modern online businesses. We combine all your sales channels,
              warehouse locations, and accounting so that every product, order,
              customer and accounts can be managed in one place.
            </BigParagraph>
          </ParagraphContainerOfSecend>
        </TopOfSecendSection>
        <BottomOfSecendSection>
          <LeftOfBottom>
            <Pixalimg src={pixalimg} />
          </LeftOfBottom>
          <RightOfBottom>
            <SimpleContainer>
              <Div>
                <SimpleImg src={inventery} />
                <SimpleDiv>
                  <SmallHeading>Inventory Management</SmallHeading>
                  <SmallParagraphContainer>
                    <SmallParagraph>
                      Keep track of your products as you sell, manufacture and
                      restock across multiple warehouse locations and channels.
                    </SmallParagraph>
                  </SmallParagraphContainer>
                </SimpleDiv>
              </Div>
              <Div>
                <SimpleImg src={order} />
                <SimpleDiv>
                  <SmallHeading>Orders & Fulfillment</SmallHeading>
                  <SmallParagraphContainer>
                    <SmallParagraph>
                      Sync orders from multiple sales channels with inventory
                      and accounting to boost business efficiency.
                    </SmallParagraph>
                  </SmallParagraphContainer>
                </SimpleDiv>
              </Div>
              <Div>
                <SimpleImg src={relationship} />
                <SimpleDiv>
                  <SmallHeading>Relationships</SmallHeading>
                  <SmallParagraphContainer>
                    <SmallParagraph>
                      Keep all your customer and supplier data in one place,
                      complete with purchase histories and customer- specific
                      insights.
                    </SmallParagraph>
                  </SmallParagraphContainer>
                </SimpleDiv>
              </Div>
              <Div>
                <SimpleImg src={intellgence} />
                <SimpleDiv>
                  <SmallHeading>Intelligence</SmallHeading>
                  <SmallParagraphContainer>
                    <SmallParagraph>
                      Generate accurate sales, inventory, business reports and
                      forecasts for better business decisions, all in real-time.
                    </SmallParagraph>
                  </SmallParagraphContainer>
                </SimpleDiv>
              </Div>
            </SimpleContainer>
          </RightOfBottom>
        </BottomOfSecendSection>
      </SecendSectionContainer>
      {/* <button>
        <FontAwesomeIcon icon={faAngleDoubleUp} />
      </button> */}
    </>
  );
}

export default SecendSection;
const SmallParagraphContainer = styled.div`
  width: 100%;
`;
const SimpleContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
`;
