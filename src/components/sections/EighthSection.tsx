import React from "react";
import selitex from "../images/salitex.png";
import florome from "../images/florome.png";
import {
  BigParagraph,
  EighthSectionContainer,
  SixthSectionHeading,
  BottonOfEight,
  ImgContainer,
  EighthContainerImg,
  SmallHeadingOfEight,
  EightSectionHeading,
} from "../../styled";
import styled from "styled-components";
function EighthSection() {
  return (
    <EighthSectionContainer>
      <EightSectionHeading>
        We're Helping Businesses Like Yours In Pakistan!
      </EightSectionHeading>
      <ParagraphContainer>
        <BigParagraph>
          Businesses like yours are loving Pixel One for orders and inventory
          control and shipping to grow their business. Well, these are just a
          few!
        </BigParagraph>
      </ParagraphContainer>
      <BottonOfEight>
        <ImgContainer>
          <EighthContainerImg src={selitex} />
          <SmallHeadingOfEight>Salitex</SmallHeadingOfEight>
        </ImgContainer>
        <ImgContainer>
          <EighthContainerImg src={florome} />
          <SmallHeadingOfEight>Salitex</SmallHeadingOfEight>
        </ImgContainer>
        <ImgContainer>
          <SmallHeadingOfEight>Al Jannat Sweets</SmallHeadingOfEight>
        </ImgContainer>
      </BottonOfEight>
    </EighthSectionContainer>
  );
}

export default EighthSection;

const ParagraphContainer = styled.div`
  width: 65%;
`;
