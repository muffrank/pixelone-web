import React from "react";
import image from "../images/saas2/screen-shot/screen.png";
import laptopimage from "../images/saas2/screen-shot/laptop.png";
import OwlCarousel from "react-owl-carousel2";
import "owl-carousel/owl-carousel/owl.carousel.css";

import {
  BottomOfFifthSection,
  FifthSectionButton,
  FifthSectionContainer,
  FifthSectionimg,
  ForthSectionHeading,
  ForthSectionParaGraph,
  TopOfFifthSection,
} from "../../styled";
import styled from "styled-components";
const options = {
  items: 3,
  nav: false,
  dots: true,
  rewind: false,
  autoplay: false,
  autoWidth: false,
  center: true,
  loop: false,
  merge: false,
  startPosition: 1,
  animateOut: false,
  margin: 100,
};
function FifthSection() {
  return (
    <FifthSectionContainer>
      <TopOfFifthSection>
        <ForthSectionHeading>
          Try Pixel One For Free To Automate Your Operations.
        </ForthSectionHeading>
        <ForthSectionParaGraph>
          Start 30 days free trial now and see what difference it makes to your
          business.
        </ForthSectionParaGraph>
        <FifthSectionButton
          href="https://pixelone.app/auth/login"
          target="_blank"
        >
          Try For Free
        </FifthSectionButton>
      </TopOfFifthSection>
      <BottomOfFifthSection className="five-section-slider">
        {/* <FifthSectionimg src={laptopimage} /> */}

        <OwlCarousel options={options}>
          <SliderImage src={image} />
          <SliderImage src={image} />
          <SliderImage src={image} />
          <SliderImage src={image} />
          <SliderImage src={image} />
          <SliderImage src={image} />
          <SliderImage src={image} />
        </OwlCarousel>
      </BottomOfFifthSection>
    </FifthSectionContainer>
  );
}

const SliderImage = styled.img`
  width: auto;
  height: 270px;
`;
const MyAfter = styled.div`
  content: "";
  background-image: url(${laptopimage});
  left: 50px;
  top: 50px;
`;
const Slider = styled(OwlCarousel)`
  background-image: url(${laptopimage});
  background-repeat: no-repeat;
  background-position: center;
  height: 300px;
`;
export default FifthSection;
