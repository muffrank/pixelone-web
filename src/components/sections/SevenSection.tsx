import React from "react";
import OwlCarousel from "react-owl-carousel2";
import "react-owl-carousel2/src/owl.carousel.css";
import styled from "styled-components";
import {
  SevenSectionContainer,
  SevenSectionHeading,
  SevenSectionParaGraph,
} from "../../styled";
const options = {
  items: 1,
  nav: true,
  rewind: true,
  autoplay: false,
};

function SevenSection() {
  return (
    <>
      <SevenSectionContainer>
        <SevenSectionHeading>Reviews</SevenSectionHeading>
        <SevenSectionParaGraph>
          Hear our success stories straight from our customers.
        </SevenSectionParaGraph>
        <SliderMainContainer>
          <OwlCarousel options={options}>
            <SliderContainer>
              <SliderHeadingContainer>
                <SliderHeading>
                  We Rely more on pixel One for managing your orders, inventory
                  and customer.We do monthly stock reconciliations as well which
                  is great.
                </SliderHeading>
              </SliderHeadingContainer>
            </SliderContainer>
            <SliderContainer>
              <SliderHeadingContainer>
                <SliderHeading>
                  Our product catalog grew from 50 SKUs to over 1,000 SKUs and
                  it would be impossible to manage without Pixal One.
                </SliderHeading>
              </SliderHeadingContainer>
            </SliderContainer>
          </OwlCarousel>
        </SliderMainContainer>
      </SevenSectionContainer>
    </>
  );
}

export default SevenSection;
const SliderMainContainer = styled.div`
  display: flex;
  flex-direction: row;
  text-align: center;
  justify-content: center;

  width: 100%;
`;
const SliderContainer = styled.div`
  /* width: 60%; */
`;
const SliderHeading = styled.h6`
  color: #fff;
  text-transform: none;
  font-weight: 300;
  letter-spacing: 1px;
  line-height: normal;
  font-family: "Poppins", sans-serif;
  font-size: 14.316px;
`;
const SliderHeadingContainer = styled.div`
  width: 50%;
  text-align: left;
`;
