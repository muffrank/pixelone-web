import React from "react";
import { Pixelheader, NavButton, MLink } from "../styled";
import Footer from "./common/Footer";
import PixelNav from "./common/Nav";
import IntegrationComponent from "./sections/IntegrationComponent";

const Integration = () => {
  return (
    <>
      <PixelNav />;
      <IntegrationComponent />
      <Footer />
    </>
  );
};

export default Integration;
