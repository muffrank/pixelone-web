import React from "react";
import styled from "styled-components";

import {
  Button,
  Container,
  Form,
  FormControl,
  Navbar,
  NavDropdown,
  Nav as NavBar,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { NavButton } from "../../styled";

function PixalNav() {
  return (
    <>
      <HeaderContainer>
        <Navbar bg="light" expand="lg">
          <HeaderContainer>
            <Container fluid>
              {/* <Navbar.Brand href="#">Navbar scroll</Navbar.Brand> */}
              <Navbar.Toggle aria-controls="navbarScroll" />
              <Navbar.Collapse id="navbarScroll">
                <NavBar
                  className="me-auto my-2 my-lg-0"
                  style={{ maxHeight: "100px" }}
                  navbarScroll
                >
                  <NavButton as={Link} to="/">
                    HOME
                  </NavButton>
                  <NavButton as={Link} to="/feature">
                    FEATURE
                  </NavButton>
                  <NavButton as={Link} to="/integration">
                    INTEGRATION
                  </NavButton>
                  <NavButton as={Link} to="/pricing">
                    PRICING
                  </NavButton>
                  <NavButton as={Link} to="/customer">
                    CUSTOMER
                  </NavButton>
                  <NavButton as={Link} to="/contact">
                    CONTACT
                  </NavButton>
                </NavBar>
              </Navbar.Collapse>
            </Container>
          </HeaderContainer>
        </Navbar>
      </HeaderContainer>
    </>
  );
}
const HeaderContainer = styled.div`
  padding-right: 170px;
  background-color: #844adb;
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end;
  /* margin: 0px 170px 0px 0px; */
  gap: 20px;
`;
const NavContaiiner = styled.div`
  background-color: #844adb;
`;
export default PixalNav;
