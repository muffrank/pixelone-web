import React from "react";
import styled, { keyframes } from "styled-components";
import btnimg from "../images/saas2/header-icon/down.png";

const AmimateBtn = () => {
  return (
    <>
      <MainContainer>
        <AnimateBtnContainer>
          <BtnAnchor>
            <AnimateBtnImg src={btnimg} />
          </BtnAnchor>
        </AnimateBtnContainer>
      </MainContainer>
    </>
  );
};

export default AmimateBtn;

const MainContainer = styled.div`
  background: transparent;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  transform: translateY(-120px);
`;
const AnimateBtnContainer = styled.div`
  height: 86px;
  width: 66px;
  background-color: #fff;
  border-radius: 50px;
  overflow: hidden;
`;
const BtnAnchor = styled.a`
  text-decoration: none;
  font-size: 13.658px;
  color: #071828;
`;
const transform = keyframes`
  from {
    transform: translate(0px);
  }

  to {
    transform: translate(2px);
  }
  
`;

const AnimateBtnImg = styled.img`
  animation: ${transform} 2s linear infinite;
  max-width: 100%;
  height: auto;
`;
