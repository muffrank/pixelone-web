import React from "react";
import styled from "styled-components";
import { SixthSectionButton } from "../../styled";
import paperplan1 from "../images/contact-1.png";
import paperplan2 from "../images/contact-2.png";
function Form() {
  return (
    <>
      <MainContainer>
        <Container>
          {" "}
          <MainHeading>GET IN TOUCH</MainHeading>
        </Container>
        <Container>
          <MainParagraph>
            Whether you've got questions or comments, or just want to say hi -
            we'd love to hear from you!
          </MainParagraph>
        </Container>
        <FormContainer>
          <MainForm>
            <MainInputContainer>
              <InputContainer>
                <Lable>Name *</Lable>
                <Input />
              </InputContainer>
              <InputContainer>
                <Lable>Phone *</Lable>
                <Input />
              </InputContainer>
            </MainInputContainer>
            <MainInputContainer>
              <InputContainer>
                <Lable>Email *</Lable>
                <Input />
              </InputContainer>
              <InputContainer>
                <Lable>Subject *</Lable>
                <Input />
              </InputContainer>
            </MainInputContainer>
            <MainInputContainer>
              <MessageInputContainer>
                <Lable>Message *</Lable>
                <Input />
              </MessageInputContainer>
            </MainInputContainer>
            <ButtonContainer>
              <SixthSectionButton>Send</SixthSectionButton>
            </ButtonContainer>
          </MainForm>
        </FormContainer>
        <PaperPlanImg1 src={paperplan1} />
        <PaperPlanImg2 src={paperplan2} />
      </MainContainer>
    </>
  );
}

export default Form;

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
  padding: 50px 0 30px;
`;
const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;
const MainHeading = styled.h2`
  margin-top: -2px;
  margin-bottom: 6px;
  letter-spacing: 1px;
  font-weight: 700;
  line-height: 0.8;
  font-family: "Work Sans", sans-serif;
  font-size: 26.5802px;
`;

const MainParagraph = styled.p`
  letter-spacing: 1px;
  font-weight: 400;
  line-height: 0.8;
  font-family: "Work Sans", sans-serif;
  font-size: 16.2901px;
`;
const PaperPlanImg1 = styled.img`
  height: 376px;
  line-height: 14px;
  opacity: 0.5;
  position: absolute;
  right: 0px;
  text-align: left;
  overflow: hidden;
  top: 37px;
  z-index: 1;
`;
const PaperPlanImg2 = styled.img`
  height: 222px;
  line-height: 14px;
  position: absolute;
  right: 243.172px;
  top: 586px;
  text-align: left;

  z-index: 1;
`;
const FormContainer = styled.div`
  display: flex;
  width: 80%;
  background-color: #fff;
  z-index: 10;
  border-radius: 15px;
  overflow: hidden;
  margin: 50px;
`;
const MainForm = styled.form`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  padding: 60px;
  width: 100%;
  font-family: "Poppins", sans-serif;
  font-size: 14px;
  color: #071828;
`;
const MainInputContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  width: 100%;
`;
const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: baseline;
  align-content: flex-start;
  width: 100%;
`;
const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 48%;
  padding-right: 15px;
  padding-left: 15px;
  padding-bottom: 50px;
  /* gap: 10px; */
`;
const MessageInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 98%;
  padding-right: 15px;
  padding-left: 15px;
  padding-bottom: 50px;
  /* gap: 10px; */
`;
const Lable = styled.label`
  color: #947bea;
  font-weight: 500;
`;
const Input = styled.input`
  width: 100%;
  height: 36.5px;
  font-weight: 400;
  font-family: "Poppins", sans-serif;
  color: #49558d;
  border: none;
  border-radius: 0;
  border-bottom: 1px solid #f1f1f1;
  outline: none;
  font-size: 15px;
`;
