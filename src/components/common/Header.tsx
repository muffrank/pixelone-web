import React from "react";
import main from "../images/main.png";
import {
  Bold,
  HeaderButton,
  HeaderContentSection,
  HeaderHeadding,
  Headerimg,
  HeaderParagraph,
  HeaderSection1,
  HeaderSection2,
  Homenav,
  MLink,
  NavButton,
  Pixelheader,
  Pixelnavv,
} from "../../styled";
import styled from "styled-components";
import AmimateBtn from "./AmimateBtn";
function Header() {
  return (
    <>
      <Pixelheader>
        <HeaderContainer>
          <Homenav>
            <NavButton>
              <MLink to="/">Home</MLink>
            </NavButton>
            <NavButton>
              <MLink to="/feature">Features</MLink>
            </NavButton>
            <NavButton>
              <MLink to="/integration">Integration</MLink>
            </NavButton>
            <NavButton>
              <MLink to="/pricing">Pricing</MLink>
            </NavButton>
            <NavButton>
              <MLink to="/customer">Customers</MLink>
            </NavButton>
            <NavButton>
              <MLink to="/contact">Contact Us</MLink>
            </NavButton>
          </Homenav>
        </HeaderContainer>
        <HeaderContentSection>
          <HeaderSection1>
            <HeaderHeadding>
              An <Bold>eCommerce Management Software </Bold>
              To Help You Take Control Of Your Business
            </HeaderHeadding>
            <HeaderParagraph>
              Products, orders, accounts, customers, and insights in one place
            </HeaderParagraph>
            <HeaderButton
              href="https://pixelone.app/auth/login"
              target="_blank"
            >
              Try For Free
            </HeaderButton>
          </HeaderSection1>
          <HeaderSection2>
            {/* <Headerimg src={img} /> */}
            <Headerimg src={main} />
          </HeaderSection2>
        </HeaderContentSection>
      </Pixelheader>
      <AmimateBtn />
    </>
  );
}
const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  padding-right: 170px;
`;
export default Header;
