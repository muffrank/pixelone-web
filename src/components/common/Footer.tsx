import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  PixelFooter,
  MediaFooterConatainer,
  LinkFooterConatiner,
  MediaContainer,
  MediaHeading,
  MediaLink,
  MediaLink1,
  MediaLink2,
  LinkContainer,
  LinkHeading,
  FooterLinkContainer,
  FooterLink,
  FooterInput,
  InputContainer,
  PaperPlane,
  InputAnchor,
  LastDiv,
  LastBoldHeading,
  LastThinHeading,
  FooterDiv,
} from "../../styled";
import { faCoffee, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
// import {
//   faFacebook,
//   faFacebookMessenger,
//   faYoutube,
// } from "@fortawesome/free-brands-svg-icons";

function Footer() {
  return (
    <>
      <PixelFooter>
        <FooterDiv>
          <MediaFooterConatainer>
            <MediaHeading>Pixel Software Solutions</MediaHeading>
            <MediaContainer>
              <MediaLink>
                {/* <MediaLink1 icon={faFacebookMessenger} /> */}
                {/* <FontAwesomeIcon icon="fa-brands fa-facebook-f" /> */}
              </MediaLink>
              <MediaLink>
                {/* <MediaLink2 icon={faFacebookMessenger} /> */}
              </MediaLink>
            </MediaContainer>
          </MediaFooterConatainer>
          <LinkFooterConatiner>
            <LinkContainer>
              <LinkHeading>PRODUCT</LinkHeading>
              <FooterLinkContainer>
                <FooterLink to="/feature"> Discover Features</FooterLink>
                <FooterLink to="/integration">Integration</FooterLink>
                <FooterLink to="/customer">Customers</FooterLink>
                <FooterLink to="/home">Free Trials</FooterLink>
              </FooterLinkContainer>
            </LinkContainer>
            <LinkContainer>
              <LinkHeading>COMPANY</LinkHeading>
              <FooterLinkContainer>
                <FooterLink to="/home">About Us</FooterLink>
                <FooterLink to="/home">User Guide</FooterLink>
                <FooterLink to="/home">Terms Of Services</FooterLink>
                <FooterLink to="/home">Privacy Policy</FooterLink>
              </FooterLinkContainer>
            </LinkContainer>
            <InputContainer>
              <LinkHeading>SUBSCRIBE OUR NEWSLETTER</LinkHeading>
              <InputAnchor>
                <FooterInput placeholder="Email Address" type="Email" />
                <PaperPlane icon={faPaperPlane} />
              </InputAnchor>
              <LastDiv>
                <LastBoldHeading>SUBSCRIBE OUR NEWSLETTER</LastBoldHeading>
                <LastThinHeading>
                  © 2021 Pixel Software Solutions
                </LastThinHeading>
              </LastDiv>
            </InputContainer>
          </LinkFooterConatiner>
        </FooterDiv>
      </PixelFooter>
    </>
  );
}

export default Footer;
