import React from "react";
import styled from "styled-components";
import Footer from "./common/Footer";
import Form from "./common/Form";
import PixelNav from "./common/Nav";
const MainConatainer = styled.div`
  background-image: linear-gradient(#543c81, #e04663);
  /* background-color: red; */
  color: #fff;
  overflow: hidden;
  padding-bottom: 25px;
`;

const Contact = () => {
  return (
    <>
      <PixelNav />
      <MainConatainer>
        <Form />
      </MainConatainer>
      <Footer />
    </>
  );
};

export default Contact;
