import React from "react";
import FourthSection from "./sections/FourthSection";
import Footer from "./common/Footer";
import PixelNav from "./common/Nav";
import { Pixelheader, NavButton, MLink } from "../styled";

const Feature = () => {
  return (
    <>
      <PixelNav />

      <FourthSection />
      <Footer />
    </>
  );
};

export default Feature;
