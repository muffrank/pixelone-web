import React from "react";
import { Pixelheader, NavButton, MLink } from "../styled";
import Footer from "./common/Footer";
import PixelNav from "./common/Nav";
import SixthSection from "./sections/SixthSection";

const Pricing = () => {
  return (
    <>
      <PixelNav />;
      <SixthSection />
      <Footer />
    </>
  );
};

export default Pricing;
