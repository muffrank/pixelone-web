import React from "react";
import Header from "./common/Header";
import SecendSection from "./sections/SecendSection";
import ThirdSection from "./sections/ThirdSection";
import FourthSection from "./sections/FourthSection";
import FifthSection from "./sections/FifthSection";
import SixthSection from "./sections/SixthSection";
import SevenSection from "./sections/SevenSection";
import EighthSection from "./sections/EighthSection";
import Footer from "./common/Footer";
// import { FontAwesomeIcon } from '@fortawesome/fontawesome-free'
import { MainCOntainer } from "../styled";

const Home = () => {
  return (
    <>
      <MainCOntainer>
        <Header />
        <SecendSection />
        <ThirdSection />
        <FourthSection />
        <FifthSection />
        <SixthSection />
        <SevenSection />
        <EighthSection />
        <Footer />
      </MainCOntainer>
    </>
  );
};

export default Home;
