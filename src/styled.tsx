import { Link } from "react-router-dom";
import styled, { keyframes } from "styled-components";
import headerbgimg from "./components/images/saas2/slider.png";
import thirdbgimg from "./components/images/saas2/background/quick-sol.png";
import fifthbgimg from "./components/images/saas2/background/screenshot-bg.png";
import backgroundbotom from "./components/images/saas2/background.png";
import testimonial from "./components/images/saas2/background/bg-testimonial.png";
import footerbgimg from "./components/images/saas2/footer.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCoffee, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { Nav } from "react-bootstrap";
const MLink = styled(Link)`
  text-decoration: none;
  color: #ffffff;
`;

const MainCOntainer = styled.body`
  ::-webkit-scrollbar {
    width: 10px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;
// ------------------------------------------------header start
const Pixelheader = styled.header`
  background-image: url(${headerbgimg});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom;
  height: 100vh;
  width: 100%;
  @media only screen and (max-width: 600px) {
    padding: 110px 0;
  }
  /* height: 500px; */
`;
const Pixelnavv = styled(Nav)`
  /* background-color: transparent;
  width: 100%; */
  /* display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end; */
  /* margin: 0px 170px 0px 0px; */
  z-index: 10;
  gap: 20px;
  padding: 15px 0px;
`;
const Homenav = styled.nav`
  background-color: transparent;
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end;
  /* margin: 0px 170px 0px 0px; */
  gap: 20px;
  padding: 15px 0px;
  @media only screen and (max-width: 600px) {
    padding: 0;
  }
`;
const NavButton = styled(Nav.Link)`
  background-color: transparent;
  font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
  font-weight: 300;
  font-size: 18px;
  cursor: pointer;
  text-decoration: none;
  /* line-height: 20px; */
  color: #fff;
  border: transparent;
`;
const HeaderContentSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  /* gap: 150px; */
  margin-top: 50px;
  width: 100%;
`;
const HeaderSection1 = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: flex-start;
  align-content: center;
  width: 38%;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;
const HeaderSection2 = styled.div`
  width: 50%;
  text-align: right;
  @media only screen and (max-width: 600px) {
    display: none;
    padding: 110px 0;
  }
`;
const HeaderHeadding = styled.h1`
  font-weight: 200;
  font-size: calc(18px + (38 - 18) * ((100vw - 300px) / (1920 - 300)));
  color: #ffffff;
  line-height: inherit;
`;
const HeaderParagraph = styled.p`
  font-weight: 300;
  color: #ffffff;
`;
const HeaderButton = styled.a`
  padding: 14px 35px;
  font-size: calc(13px + (16 - 13) * ((100vw - 300px) / (1920 - 300)));
  font-weight: 500;
  text-decoration: none;
  border: 2px solid;
  text-transform: capitalize;
  border-radius: 50px;
  color: #fff;
  transition: 0.5s;
  letter-spacing: 0;
  white-space: nowrap;
  cursor: pointer;
  background: transparent;
  border: 1px solid #fff;
  :hover {
    color: #8e6cde;
    background-color: #fff;
  }
  @media only screen and (max-width: 600px) {
    padding: 10px 25px;
  }
`;

const Headerimg = styled.img`
  height: 300px;
`;

const Bold = styled.b`
  font-size: 31.1605px;
  font-weight: 400;
`;
// ------------------------------------------------------------- header end

//------------------------------------------------------------secend section start
const SecendSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: baseline;
  align-items: center;
  align-content: center;
  background-image: url(${backgroundbotom});
  position: relative;
  overflow: hidden;
  background-size: contain;
  background-position: right;
  padding: 70px 0;
  width: 100%;
`;
const TopOfSecendSection = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
`;
const BottomOfSecendSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  /* gap: 80px; */
  width: 100%;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
  }
`;
const BigHeading = styled.h2`
  line-height: 0.9;
  color: #844adb;
  font-family: "Poppins", sans-serif;
  text-transform: capitalize;
  font-size: calc(20px + (33 - 20) * ((100vw - 300px) / (1920 - 300)));
`;
const BigParagraph = styled.p`
  color: #071828;
  font-size: calc(13px + (16 - 13) * ((100vw - 300px) / (1920 - 300)));
  margin-top: 0;
  margin-bottom: -3px;
  line-height: 1.8;
  text-align: center;
`;
const ParagraphContainerOfSecend = styled.div`
  width: 68%;
  @media only screen and (max-width: 600px) {
    width: 93%;
  }
`;
const SmallHeading = styled.h5`
  font-size: 17.316px;
  font-weight: 500;
  color: #844adb;
  font-family: "Poppins", sans-serif;
  margin-bottom: 8px;
`;
const SmallParagraph = styled.p`
  font-size: 15px;
  color: #606a84;
  line-height: 26px;
`;
const LeftOfBottom = styled.div`
  width: 50%;
  @media only screen and (max-width: 600px) {
    width: 100%;
    text-align: center;
  }
`;
const RightOfBottom = styled.div`
  width: 29%;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;
const Pixalimg = styled.img`
  position: relative;
  z-index: 1;
  margin-bottom: -20px;
  left: 100px;
  top: 15px;
  @media only screen and (max-width: 600px) {
    left: 0;
    top: 0;
    height: 400px;
    margin-bottom: 30px;
  }
`;
const Div = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    text-align: center;
    margin-bottom: 20px;
  }
`;
const SimpleDiv = styled.div`
  margin-left: 30px;
  width: 100%;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    text-align: center;
  }
`;
const SimpleImg = styled.img`
  height: 35px;
`;
//----------------------------------------------------secend section end

//---------------------------------------------------third section start
const ThirdSectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  /* align-content: ; */
  overflow: hidden;
  padding: 70px 0 70px 0;
  width: 100%;
  background-image: url(${thirdbgimg});
  background-repeat: no-repeat;
  position: relative;
`;
const ChekCircle = styled(FontAwesomeIcon)`
  color: #fff;
  font-size: 20px;
`;
const LeftOfThirdSection = styled.div`
  width: 41%;
  @media only screen and (max-width: 600px) {
    width: 90%;
  }
`;
const RightOfThirdSection = styled.div`
  width: 30%;
  @media only screen and (max-width: 600px) {
    display: none;
  }
`;
// const ThirdSectionDiv = styled.div`
//   display: flex;
//   flex-direction: row;
//   flex-wrap: nowrap;
//   justify-content: center;
//   align-items: center;
//   align-content: center;
//   @media only screen and (max-width: 600px) {
//     flex-direction: column;
//     text-align: center;
//     margin-bottom: 20px;
//   }
// `;
const ThirdSectionSimpleDiv = styled.div`
  margin-left: 30px;
  width: 100%;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    text-align: inherit;
  }
`;
const ThirdMainHeading = styled.h1`
  font-size: calc(20px + (33 - 20) * ((100vw - 300px) / (1920 - 300)));
  margin-bottom: 25px;
  font-weight: 400;
  color: #ffffff;
`;
const ThirdSectionDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  margin-bottom: 40px;
  @media only screen and (max-width: 600px) {
    flex-direction: row;
    width: 100%;
  }
`;
const Border = styled.div`
  border: 2px solid #fff;
  width: 83px;
  margin-bottom: 70px;
`;

const ThirdSectionImg = styled.img`
  height: 500px;
  padding: 125px 0px 0 0;
`;
const WhiteHeading = styled.h4`
  margin-bottom: 15px;
  letter-spacing: 1px;
  font-family: "Poppins", sans-serif;
  font-size: 18.6321px;
  font-weight: 300;
  color: #fff;
`;
const WHiteParagraph = styled.p`
  font-size: calc(13px + (16 - 13) * ((100vw - 300px) / (1920 - 300)));
  margin-top: 0;
  margin-bottom: -3px;
  font-weight: 300;
  color: #fff;
`;
//----------------------------------------third section end

//-------------------------------------------------fourth section start
const FourthSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: baseline;
  align-items: center;
  align-content: center;
  padding: 70px 0;
`;
const TopOfFourthSection = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;
const BottomOfFourthSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: flex-start;
  align-content: flex-start;
  gap: 30px;
  margin-top: 50px;
  width: 100%;
`;
const FourthSectionDiv = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  background: rgba(244, 246, 250, 0.75);
  border: none;
  padding: 30px;
  box-shadow: 0 0 10px 5px rgb(253 248 247 / 69%);
  border-radius: 10px;
  /* width: 300px; */
  width: 26%;
  text-align: center;

  @media only screen and (max-width: 600px) {
    width: 94%;
  }
`;
const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
  
`;
const FourthSectionImg = styled.img`
  height: 115px;

  :hover {
    animation: ${rotate} 1s linear infinite;
  }
`;
//--------------------------------------------------------fourth section end

//---------------------------------------------------------fifth section start
const FifthSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: baseline;
  align-items: center;
  align-content: center;
  /* margin-top: 50px; */
  padding: 70px 0;
  background-image: url(${fifthbgimg});
  width: 100%;
`;
const TopOfFifthSection = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
  @media only screen and (max-width: 600px) {
    width: 94%;
    text-align: center;
  }
`;
const BottomOfFifthSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  gap: 30px;
  margin-top: 50px;
  width: 100%;
`;
const ForthSectionHeading = styled.h2`
  color: #fff;
  line-height: 0.9;
  font-size: 24px;
  font-weight: 500;
  font-size: 28px;
  /* padding-top: 50px; */
  font-family: "Poppins", sans-serif;
  @media only screen and (max-width: 600px) {
    font-size: 21.6049px;
    font-weight: 500;
    line-height: 30.2469px;
  }
`;
const ForthSectionParaGraph = styled.p`
  color: #fff;
  line-height: 0.9;
  font-size: 15px;
  font-family: "Poppins", sans-serif;
  font-weight: 400;
  @media only screen and (max-width: 600px) {
    font-size: 13.3704px;
    font-weight: 400;
    line-height: 24.0667px;
  }
`;
const FifthSectionButton = styled.a`
  padding: 14px 35px;
  font-size: calc(13px + (16 - 13) * ((100vw - 300px) / (1920 - 300)));
  font-weight: 500;
  border: 2px solid;
  text-decoration: none;
  text-transform: capitalize;
  border-radius: 50px;
  color: #fff;
  transition: 0.5s;
  letter-spacing: 0;
  white-space: nowrap;
  cursor: pointer;
  background: transparent;
  border: 1px solid #fff;
  :hover {
    color: #8e6cde;
    background-color: #fff;
  }
  @media only screen and (max-width: 600px) {
    padding: 10px 25px;
  }
`;
const FifthSectionimg = styled.img`
  height: 300px;
`;
//-----------------------------------------------------------fifth section end

//------------------------------------------------------------sixth section start
const SixthSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-items: center;
  padding: 70px 0;
  gap: 15px;
  width: 100%;
  @media only screen and (max-width: 600px) {
    text-align: center;
    gap: 35px;
    width: 90%;
  }
`;
const SixthSectionHeading = styled.h2`
  box-sizing: border-box;
  color: #844adb;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 28.5543px;
  font-weight: 500;
  height: 25.6875px;
  line-height: 25.6989px;
  /* margin-top: 10px; */
  @media only screen and (max-width: 600px) {
    font-size: 21.6049px;
    font-weight: 500;
    line-height: 30.2469px;
    text-align: center;
  }
`;
const SixthSectionButton = styled.a`
  padding: 14px 35px;
  font-size: 14.9741px;
  font-weight: 500;
  text-decoration: none;
  border: 2px solid;
  text-transform: capitalize;
  border-radius: 50px;
  margin-bottom: 10px;
  color: #fff;
  transition: 0.5s;
  letter-spacing: 0;
  white-space: nowrap;
  cursor: pointer;
  background-color: #8e6cde;
  border: 1px solid #8e6cde;
  :hover {
    color: #8e6cde;
    background-color: #fff;
  }
  @media only screen and (max-width: 600px) {
    padding: 10px 25px;
    margin-bottom: 0;
  }
`;
//-----------------------------------------------------------sixth section end

//-------------------------------------------------------------seven Section start
const SevenSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  padding: 70px 0;
  background-image: url(${testimonial});
`;
const SevenSectionHeading = styled.h2`
  color: #fff;
  line-height: 0.9;
  font-size: 24px;
  font-weight: 500;
  font-size: 28px;
  /* padding-top: 50px; */
  font-family: "Poppins", sans-serif;
`;
const SevenSectionParaGraph = styled.p`
  color: #fff;
  line-height: 0.9;
  font-size: 14.9741px;
  font-family: "Poppins", sans-serif;
  font-weight: 400;
`;
//--------------------------------------------------------seven section end
//--------------------------------------------------------eight section start
const EighthSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  width: 100%;
  padding: 70px 0;
  @media only screen and (max-width: 600px) {
    gap: 30px;
  }
`;
const BottonOfEight = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  align-content: center;
  gap: 130px;
  padding-top: 35px;
  @media only screen and (max-width: 600px) {
    padding: 10px 25px;
    margin-bottom: 0;
    flex-direction: column;
    gap: 30px;
  }
`;
const EightSectionHeading = styled.h2`
  box-sizing: border-box;
  color: #844adb;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 28.5543px;
  font-weight: 500;
  height: 25.6875px;
  line-height: 25.6989px;
  /* margin-top: 10px; */
  @media only screen and (max-width: 600px) {
    font-size: 21.6049px;
    font-weight: 500;
    line-height: 30.2469px;
    text-align: center;
  }
`;
const ImgContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media only screen and (max-width: 600px) {
    text-align: center;
  }
`;
const EighthContainerImg = styled.img`
  height: 70px;
  @media only screen and (max-width: 600px) {
    height: 140px;
  }
`;
const SmallHeadingOfEight = styled.h4`
  font-size: 18.6321px;
  color: #8e6cde;
  font-family: "Poppins", sans-serif;
`;
//--------------------------------------------------------eight section end
//--------------------------------------------------------footer start
const PixelFooter = styled.footer`
  background: url(${footerbgimg}) no-repeat top;
  background-size: cover;
  /* padding: 0 0; */
  margin-bottom: 100px;
  /* gap: 170px; */
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
  @media only screen and (max-width: 600px) {
    width: 100%;
    background: linear-gradient(#6570ea, #8e6cde);
  }
  /* height: 500px; */
`;
const FooterDiv = styled.div`
  /* background: url(${footerbgimg}) no-repeat top; */
  background-size: cover;
  padding: 180px 0 70px 0;
  /* margin-top: 30px; */
  /* gap: 170px; */
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
  width: 100%;
  /* height: 500px; */
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    justify-content: center;
  }
`;
const MediaFooterConatainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 30%;
  @media only screen and (max-width: 600px) {
    width: 100%;
    align-items: baseline;
  }
`;
const LinkFooterConatiner = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  gap: 100px;
  align-items: center;
  align-content: center;
  width: 70%;
  @media only screen and (max-width: 600px) {
    flex-direction: column;
    justify-content: center;
    width: 100%;
    align-items: baseline;
  }
`;
const MediaContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: center;
  align-content: center;
`;
const MediaHeading = styled.h6`
  color: #fff;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 14.316px;
  font-weight: 200;
`;
const MediaLink = styled.a`
  text-decoration: none;
`;
const MediaLink1 = styled(FontAwesomeIcon)`
  text-decoration: none;
  padding: 20px;
  width: 55px;
  text-align: center;
  border-radius: 50%;
  background: #3b5998;
  color: white;
  font-size: 16px;
`;
const MediaLink2 = styled(FontAwesomeIcon)`
  text-decoration: none;
  padding: 20px;
  width: 55px;
  text-align: center;
  border-radius: 50%;
  background: #bb0000;
  color: white;
  font-size: 16px;
`;
const LinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-around;
  align-items: baseline;
  align-content: center;
`;
const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: baseline;
  gap: 24px;
  align-items: baseline;
  align-content: center;
`;

const LinkHeading = styled.h5`
  color: #fff;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 17.316px;
  font-weight: 500;
`;
const FooterLinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-around;
  gap: 15px;
  align-items: baseline;
  align-content: center;
`;

const FooterLink = styled(Link)`
  color: #fff;
  cursor: pointer;
  display: inline;
  text-decoration: none;
  font-family: "Poppins", sans-serif;
  font-size: 13.658px;
  font-weight: 500;
  padding-bottom: 6px;
  :hover {
    color: #fff;
  }
`;
const FooterInput = styled.input`
  border: 0px;
  /**/

  color: #fff;
  outline: none;
  font-size: 15px;
  font-weight: 400;
  background: transparent;
  width: 220.5px;

  ::placeholder {
    color: #a798ef;
    font-size: 12px;
  }
`;
const PaperPlane = styled(FontAwesomeIcon)`
  color: #fff;
  cursor: pointer;
`;
const InputAnchor = styled.a`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  text-decoration: #fff;
  border-bottom: 1px solid #fff;
  @media only screen and (max-width: 600px) {
    width: 100%;
  }
`;
const LastDiv = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  transform: translateY(54px);
`;
const LastBoldHeading = styled.h5`
  color: #fff;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 17.316px;
  font-weight: 500;
  margin-bottom: 0;
`;
const LastThinHeading = styled.h6`
  color: #fff;
  display: block;
  font-family: "Poppins", sans-serif;
  font-size: 14.316px;
  font-weight: 200;
  margin-top: 0;
`;
//---------------------------------------------------------footer end

export {
  MainCOntainer,
  Homenav,
  //--header
  Pixelheader,
  Pixelnavv,
  NavButton,
  HeaderContentSection,
  HeaderHeadding,
  HeaderSection1,
  HeaderSection2,
  HeaderParagraph,
  HeaderButton,
  Headerimg,
  Bold,
  MLink,
  //--secend section
  SecendSectionContainer,
  TopOfSecendSection,
  BottomOfSecendSection,
  ParagraphContainerOfSecend,
  LeftOfBottom,
  RightOfBottom,
  BigHeading,
  BigParagraph,
  SmallHeading,
  SmallParagraph,
  Pixalimg,
  Div,
  SimpleDiv,
  SimpleImg,
  //--third section
  ThirdSectionContainer,
  ThirdMainHeading,
  LeftOfThirdSection,
  RightOfThirdSection,
  Border,
  ThirdSectionDiv,
  ThirdSectionImg,
  WhiteHeading,
  WHiteParagraph,
  ThirdSectionSimpleDiv,
  //--fourth section
  FourthSectionContainer,
  TopOfFourthSection,
  BottomOfFourthSection,
  FourthSectionDiv,
  FourthSectionImg,
  //--fifth section
  FifthSectionContainer,
  TopOfFifthSection,
  BottomOfFifthSection,
  ForthSectionHeading,
  ForthSectionParaGraph,
  FifthSectionButton,
  FifthSectionimg,
  //--sixth section
  SixthSectionContainer,
  SixthSectionHeading,
  SixthSectionButton,
  //--seven section
  SevenSectionContainer,
  SevenSectionHeading,
  SevenSectionParaGraph,
  //--eight Section
  EighthSectionContainer,
  BottonOfEight,
  ImgContainer,
  EighthContainerImg,
  SmallHeadingOfEight,
  EightSectionHeading,
  //--footer
  PixelFooter,
  MediaFooterConatainer,
  LinkFooterConatiner,
  MediaContainer,
  MediaHeading,
  MediaLink,
  MediaLink1,
  MediaLink2,
  LinkContainer,
  LinkHeading,
  FooterLinkContainer,
  FooterLink,
  InputContainer,
  FooterInput,
  PaperPlane,
  InputAnchor,
  LastDiv,
  LastBoldHeading,
  LastThinHeading,
  ChekCircle,
  FooterDiv,
};
