import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import Home from "./components/Home";
import Feature from "./components/Feature";
import Integration from "./components/Integration";
import Pricing from "./components/Pricing";
import Customer from "./components/Customer";
import Contact from "./components/Contact";
import reportWebVitals from "./reportWebVitals";
import {
  BrowserRouter as Router,
  Route,
  Link,
  BrowserRouter,
  Routes,
} from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="feature" element={<Feature />} />
        <Route path="integration" element={<Integration />} />
        <Route path="pricing" element={<Pricing />} />
        <Route path="customer" element={<Customer />} />
        <Route path="contact" element={<Contact />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
